const jwt = require('jsonwebtoken');

module.exports = {
    login: async (req, res) => {
        if (req.method === 'GET') {
            res.view('pages/login', { messages: [], username: '', password: '' });
        } else if (req.method === 'POST') {

            const username = req.body.username;
            const password = req.body.password;
            let error = [];

            if (username != '' && password != '') { // 帳號和密碼都有輸入

                try {
                    var user = await User.findOne({ username: username });
                    if (user) { // 有找到用戶
                        if (user.password != password) { // 密碼有誤
                            error.push('Password Error!');
                        }
                    } else { // 沒找到用戶
                        error.push('Not found this username!');
                    }
                } catch (err) { // 資料庫搜尋時有誤
                    error.push('DataBase Error!');
                }

            } else { // 帳號和密碼沒輸入完全

                if (username == '')
                    error.push('Please enter username!');
                if (password == '')
                    error.push('Please enter password!');
            }

            if (error.length > 0) // 有錯誤
                res.view('pages/login', { messages: error, username: username, password: password });
            else { // 沒錯誤
                const token = 'Bearer ' + jwt.sign(
                    {
                        'id': user.id,
                        'fullname': user.fullname
                    },
                    sails.config.custom.jwtKey,
                    {
                        expiresIn: 60 * 15
                    }
                );
                res.cookie('TodoToken', token, { httpOnly: true, maxAge: 60 * 15 * 1000 });
                res.redirect('/');
            }
        }
    },
    register: async (req, res) => {
        const method = req.method;

        if (method === 'GET') {
            res.view('pages/register', { messages: [], username: '', password: '', fullname: '' });
        } else if (method === 'POST') {

            const username = req.body.username;
            const password = req.body.password;
            const fullname = req.body.fullname;
            let error = [];

            if (username != '' && password != '' && fullname != '') {

                try {
                    await User.create({ username: username, password: password, fullname: fullname });
                } catch (err) {
                    if (err.raw.code == 11000)
                        error.push('Have the same username!');
                    else
                        error.push('DataBase Error!');
                }
            } else {
                if (username == '')
                    error.push('Please enter username!');
                if (password == '')
                    error.push('Please enter password!');
                if (fullname == '')
                    error.push('Please enter fullname!');
            }

            if (error.length > 0)
                res.view('pages/register', { messages: error, username: username, password: password, fullname: fullname });
            else
                res.redirect('/login');
        }
    },
    logout: async (req, res) => {
        res.clearCookie('TodoToken');
        res.redirect('/login');
    }
}
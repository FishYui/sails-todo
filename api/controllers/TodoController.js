module.exports = {
    list: async (req, res) => {
        const userId = req.user.id;
        
        try {
            const todos = await Todo.find({ owner: userId });
            res.view('pages/homepage', { username: req.user.fullname, todos: todos });
        } catch (err) {
            res.send(500, { error: 'DataBase Error!' });
        }
    },
    create: async (req, res) => {
        const userId = req.user.id;
        const title = req.body.title;

        try {
            await Todo.create({ title: title, owner: userId });
            res.redirect('/');
        } catch (err) {
            res.send(500, { error: 'DataBase Error!' });
        }
    },
    delete: async (req, res) => {
        const id = req.params.id;

        try {
            await Todo.destroy({ id: id });
            res.redirect('/');
        } catch (err) {
            res.send(500, { error: 'DataBase Error!' });
        }
    },
    update: async (req, res) => {
        const id = req.params.id;
        const status = req.params.status === 'true' ? true : false;

        try {
            await Todo.update({ id: id }, { status: !status });
            res.redirect('/');
        } catch (err) {
            res.send(500, { error: 'DataBase Error!' });
        }
    }
}
module.exports = {
    attributes: {
        title: {
            type: 'string'
        },
        status: {
            type: 'boolean',
            defaultsTo: false
        },
        owner: {
            model: 'user'
        }
    }
}
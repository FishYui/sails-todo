module.exports = {
    attributes: {
        username: {
            type: 'string',
            required: true,
            allowNull: false,
            unique: true
        },
        password: {
            type: 'string',
            required: true,
            allowNull: false
        },
        fullname: {
            type: 'string',
            required: true,
            allowNull: false
        },
        todos: {
            collection: 'todo',
            via: 'owner'
        }
    }
}